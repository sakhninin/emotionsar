package com.example.ninasakhnini.emotionsar;

import android.net.Uri;

import com.nuance.speechkit.PcmFormat;

/**
 * All Nuance Developers configuration parameters can be set here.
 *
 * Copyright (c) 2015 Nuance Communications. All rights reserved.
 */
public class Configuration {

    public static final String APP_KEY = "9d5ff09a320ea70f84a0840a8860b15453473878c38618b4f30af942fd90c39aefb33422c1db31467bb5500315015769488fe10c15f9c00904ba4b014a0f9b39";
    public static final String APP_ID = "NMDPTRIAL_sakhnini_nina_gmail_com20170828194115";

    // Server
    public static final String SERVER_HOST = "nmsps.dev.nuance.com";
    public static final String SERVER_PORT = "443";
    public static final Uri SERVER_URI = Uri.parse("nmsps://" + APP_ID + "@" + SERVER_HOST + ":" + SERVER_PORT);

    // Language
    public static final String LANGUAGE = "eng-USA";

    // Only needed if using NLU
    public static final String CONTEXT_TAG = "M8522_A2900_V1";//All fields are required.

    public static final PcmFormat PCM_FORMAT = new PcmFormat(PcmFormat.SampleFormat.SignedLinear16, 16000, 1);
    public static final String LANGUAGE_CODE = (Configuration.LANGUAGE.contains("!") ? "eng-USA" : Configuration.LANGUAGE);

}



