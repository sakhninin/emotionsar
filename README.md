EmotionsAR
EmotionsAR is a project that explores representing emotions from everyday speech using an avatar. Facial expressions play an important role in conversations. Facial expressions help better connect and communicate people within a conversation. In some situations, such as, phone calls, facial expressions are absent. This sometimes lead to misunderstanding and might leading to underestimation or overestimation to what is being communicated. This misunderstanding can sometimes lead to problems (and fights).
This project aims to reduce the absence of facial expressions by creating and augmenting an avatar in the same room with the user. The user can have a better experience by seeing the expressions of certain conversation represented by the avatar.
This project explores creating an augmented reality application that extracts basic (For the scope of this project) emotions from a conversation. These emotions will be represented to the user as an avatar�s facial expressions. The avatar will be placed in the room where the user is. For the scope of this project, the avatar is Andy, Android�s symbolic avatar.
The goal of this project is to develop an AR application that takes a speech and translates the feelings of the person speaking into a 3D avatar in the room of the user. The 3D avatar will have facial expressions in accord with the speaker�s feelings. 
For the scope of this project, there are three facial expressions: Happy, sad, and neutral. For the happy avatar, a green Andy with a big smile was chosen. The happy Andy is shown in the figure below:
Figure 1: Happy Andy andyhappy.png
For the sad avatar, an orange Andy with a frown and eyebrows was chosen. The sad Andy is shown in the figure below.
Figure 2: Sad Andy andysad.png
Finally, for the neutral feeling, a blue Andy with no facial expressions is shown. The neutral Andy is shown in the figure below.
Figure 3: Neutral Andy andyneutral.png
Why AR? Having the avatar in the same room would increase the comfort of the user. Also, it would add a feeling of �hearth� to the conversation. Above all, it is fun, just like Snapchat�s Bitmoji 3D avatars.
How does the application work?
The application is launched by tapping the application�s icon in the mobile device. Shown in the figure below.
Figure 4: Launch launch.png
Next, the camera will be turned on and the application will start searching for surfaces. Points pointing to the detected point cloud will be shown on the screen. Shown in the figure below.
Figure 5: Searching for surfaces searchsurface.png
After that, when a surface is detected, a mesh is drawn. The figure below shows the detected surface.
Figure 6: Detected Surface surface.png
In order for the user to see the avatar, he/she must tap on the area of the surface where he/she likes to see the avatar in. By tapping there, the avatar will appear. The user can see up to two avatars at the same time, just for the fun of it.
Figure 7: Avatar avatartap.png
After that, the avatar will be changing according to the feeling extracted from the speech. The user will be speaking normally and the application will be processing the speech and extracting the feelings and reflecting them on the avatar. Until this point, the accuracy of the emotion detection model is fairly good. But, it still needs improving.
The target platform for this application is mobile devices that run Android 7.0 Nougat or higher. Also, the devices should support AR. That is, by having multiple rear cameras and support Google�s tango. 
Notes on running this project:
1)	The user should enable the microphone, by giving the application the permission to use it.
2)	Downloading ARCore Service on the mobile device might be needed to run the application. https://github.com/google-ar/arcore-android-sdk/releases/download/sdk-preview/arcore-preview.apk
Development platform:
1)	Android Studio: https://developer.android.com/studio/index.html?gclid=Cj0KCQiAgZTRBRDmARIsAJvVWAugsxgHivxcnG_AFng1DnNTzYV_fx9rnShYEy4yfb5BmYD_Jq2EuYgaAnYMEALw_wcB
Devices used in development:
1)	Samsung Galaxy S8
Challenges:
As per what was the plan at the proposal time, there are multiple main challenges. The first challenge was using an avatar of a real person using Avatar SDK (www.avatarsdk.com). I was intending to usd the SDK to upload the person�s selfie and create an avatar using that selfie, then, use the avatar to express the emotions.
The second challenge was to run the application at the same time with a phone call. The plan was to run the application as a phone-call application where it takes the voice coming of the phone and reflects the emotions. I tried using the normal cellular phone calls as well as VoIP.
The third challenge was, developing on both unity and Android Studio and combining the works together. I was planning to develop a part of the application using Unity, since unity allows exporting Android projects, and a part of the application using Android studio and then combining both works together into one application. 
What did Work:
What did work is running the application for real conversation, not transmitted through the phone. Also, using a standard avatar was essential at this point of developing the application.
The application includes a login and register page connected to firebase database where users can actually register and login to their accounts. Also, the application has a contact list with dummy content at this point. These two parts are hidden at this version of the application. They will be out for the next version when the VoIP calls will be implemented.
What did not:
The Avatar SDK was in beta version and doesn�t have enough documentation. Also, it is only for Unity (among the IDEs I am working with). I could not combine a Unity and an Android studio codes together. So, I had to give up on using Avatar SDK. 
The other problem was with the phone calls. There was a resource allocation problem that lead to being unable to use the application at the same time with a cellular phone call. For the VoIP calls, I am still learning how to do it.
Future Work:
For the future work, as mentioned earlier, VoIP calls will be included to the application. Also, if Avatar SDK release a version for Android, then, the avatar will be replaced.
Another aspect of improvement is improving the accuracy of the algorithm detecting the emotions as well as that more emotions will be extracted to add a level of accuracy.
In the future, this application might become a part of a lot of phone calls made around the world. Also, this application might inform the design of a new chat application using AR.
Resources:
1)	ARCore SDK: https://developers.google.com/ar/
2)	Nuance Dragon Dictation Mix.nlu: https://developer.nuance.com/public/index.php?task=mix
3)	NRC Word-Emotion Association Lexicon: http://saifmohammad.com/WebPages/NRC-Emotion-Lexicon.htm (This application makes use of the NRC Word-Emotion Association Lexicon, created by Saif Mohammad and Peter Turney at the National Research Council Canada.)
4)	Datasets from the Shared Task on Emotion Intensity: http://saifmohammad.com/WebPages/EmotionIntensity-SharedTask.html (This application makes use of the NRC Word-Emotion Association Lexicon, created by Saif Mohammad and Felipe Bravo-Marquez Turney at the National Research Council Canada.)
5)	Firebase: https://console.firebase.google.com/u/0/
6)	Logo: https://www.iconfinder.com/icons/2633207/electronics_gaming_oculus_rift_technology_virtual_reality_icon#size=128
7)	Twitter Sentiment Analysis Database: http://thinknook.com/wp-content/uploads/2012/09/Sentiment-Analysis-Dataset.zip
8)	Video: Short story: Rise and Fall http://www.kidsworldfun.com/very-short-stories-for-children.php
9)	Video: Short story TTS narration: https://translate.google.com/#en/ar/

Link to Code:
	https://bitbucket.org/sakhninin/emotionsar/
Link to APK:
https://bitbucket.org/sakhninin/emotionsar/src/e16d6073b2fb65e5ca5d8c199c1afa1e046ef582/APK/?at=master
Video:
https://youtu.be/T-JvXoFn2G4